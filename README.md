# Liza [LIZA]

*Liza is an x13 proof of stake cryptocurrency.* 

To connect to the Liza network use **rpcport 5515** and **p2p port 5516**.


# Benefits of Liza

- Liza allows Yobit/other users to invest in a crptocurrency with a blockchain.
- Owners of Liza virtual token on Yobit can swap those tokens for Liza cryptocurrency on a 10:1 basis (10 Yobit Liza's = 1 blockhain Liza)
- The Liza masternode programme will pay nodes in Liza monthly!
- Mining will be reintroduced when Liza has been established as a cryptocurrency.
- Liza will be a good investment opportunity and pays 2000% annually.



![i](https://cdn.pbrd.co/images/HDoVaPv.png)

# Connecting

If you do not connect to the Liza network automatically, use these nodes in a Liza.conf: 45.58.48.93:5516, 45.76.47.47:5516 and 45.76.191.103:5516.

# Liza wallet


[Download the Liza wallet here](https://bitbucket.org/LizaDarling/lizadarling/downloads/Liza-qt.zip).


# Exchange

Liza will be listed on [Altsmarkets Exchange](https://altmarkets.cc/?ref=621).

----------
