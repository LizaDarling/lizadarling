#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/Liza.ico

convert ../../src/qt/res/icons/Liza-16.png ../../src/qt/res/icons/Liza-32.png ../../src/qt/res/icons/Liza-48.png ${ICON_DST}
